package com.ewewerka.tasklist.register;

import com.ewewerka.tasklist.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("validateRegistrationForm")
public class ValidateRegisterForm implements Validator {

    private final UserService userService;

    public ValidateRegisterForm(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterForm registerForm = (RegisterForm) o;

        if(StringUtils.isBlank(registerForm.getLogin())){
            errors.rejectValue("login", "form.field.empty");
        }

        if(userService.userExistByLogin(registerForm.getLogin())){
            errors.rejectValue("login", "form.login.exists");
        }

        if(StringUtils.isBlank(registerForm.getName())){
            errors.rejectValue("name", "form.field.empty");
        }
        if(StringUtils.isBlank(registerForm.getLastName())){
            errors.rejectValue("lastName", "form.field.empty");
        }
        if(StringUtils.isBlank(registerForm.getPassword())){
            errors.rejectValue("password", "form.field.empty");
        }
        if(StringUtils.isBlank(registerForm.getRepeatPassword())){
            errors.rejectValue("repeatPassword", "form.field.empty");
        }
        if (!registerForm.getPassword().equals(registerForm.getRepeatPassword())) {
            errors.rejectValue("repeatPassword", "form.password.not.equals");
        }
    }

}
