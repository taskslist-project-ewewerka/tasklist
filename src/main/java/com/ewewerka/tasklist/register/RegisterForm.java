package com.ewewerka.tasklist.register;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RegisterForm {
    private String login;
    private String mail;
    private String name;
    private String lastName;
    private String password;
    private String repeatPassword;

}
