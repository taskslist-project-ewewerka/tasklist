package com.ewewerka.tasklist.register;

import com.ewewerka.tasklist.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserService userService;
    private final Validator validator;

    @InitBinder
    void initBinder(WebDataBinder binder) {
    binder.addValidators(validator);
    }
    @Autowired
    public RegisterController(UserService userService, @Qualifier("validateRegistrationForm") Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    @RequestMapping
    ModelAndView getRegisterPage() {
        ModelAndView mnv = new ModelAndView("registerPage");
        mnv.addObject("registerForm", new RegisterForm());
        return mnv;
    }

    @PostMapping
    String takeDataFromUser(@ModelAttribute(name = "registerForm") @Validated RegisterForm registerForm,
                            BindingResult bindingResult, Model model) {

        model.addAttribute("registerForm", registerForm);

        if(bindingResult.hasErrors()){
            return "registerPage";
        }
        userService.createUser(registerForm);
        return "redirect:/login";
    }
}
