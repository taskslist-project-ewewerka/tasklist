package com.ewewerka.tasklist.admin;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.user.UserDto;
import com.ewewerka.tasklist.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping
    String getAdminPage(Principal principal, Model model) throws NoUserException {
        String login = principal.getName();
        String name = userService.findUserByLogin(login).getName();
        model.addAttribute("userName", name);
        return "adminPage";
    }

    @RequestMapping("/users")
    ModelAndView getViewUsersPage(Principal principal) throws NoUserException {
        ModelAndView mnv = new ModelAndView("viewRegisteredUsers");
        mnv.addObject("users", userService.registeredUsers());
        mnv.addObject("numberOfUsers", userService.countAllUsers());
        String login = principal.getName();
        String name = userService.findUserByLogin(login).getName();
        mnv.addObject("userName", name);
        return mnv;

    }

}


