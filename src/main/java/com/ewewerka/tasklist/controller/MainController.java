package com.ewewerka.tasklist.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/main")
public class MainController {

    @RequestMapping
    String getAdminOrUserPage(Principal principal) {
        String name = principal.getName();
        if (name.equals("admin")) {
            return "redirect:/admin";
        } else
            return "redirect:/tasks";

    }

}
