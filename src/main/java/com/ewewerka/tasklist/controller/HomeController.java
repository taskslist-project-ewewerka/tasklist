package com.ewewerka.tasklist.controller;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.user.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/index")
public class HomeController {
    private final UserService userService;

    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping
    public ModelAndView getIndexPage() throws NoUserException {
        ModelAndView mnv = new ModelAndView("index");
        getUserNameIfAuthenticated(mnv);
        return mnv;
    }

    private void getUserNameIfAuthenticated(ModelAndView mnv) throws NoUserException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        if(isAuthenticatedUser(login)) {
            String name = userService.findUserByLogin(login).getName();
            mnv.addObject("userName", name);
        }
    }

    private boolean isAuthenticatedUser(String login) {
        return login != null && !login.isEmpty() && !login.equals("anonymousUser");
    }
}
