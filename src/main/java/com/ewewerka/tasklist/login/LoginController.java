package com.ewewerka.tasklist.login;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class LoginController {

    @RequestMapping
    ModelAndView getLoginPage(@RequestParam(value = "error", required = false) String error, Model model) {
        if(error != null) {
            model.addAttribute("errormessage", "Nieprawidłowy login lub hasło");
        }
        ModelAndView mnv = new ModelAndView("loginPage");
        mnv.addObject("loginForm", new LoginForm());
        return mnv;
    }

    @RequestMapping(value = "/logout")
    public String logoutPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            auth = null;
        }
        return "redirect:/loginPage";
    }
}
