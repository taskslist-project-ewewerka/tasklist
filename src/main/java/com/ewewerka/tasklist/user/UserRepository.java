package com.ewewerka.tasklist.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> getByLogin(String login);

    boolean existsByLogin(String login);

    List<UserEntity> findAll();

//    int countAll();

}
