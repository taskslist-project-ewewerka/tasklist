package com.ewewerka.tasklist.user;

import com.ewewerka.tasklist.task.TaskEntity;
import com.ewewerka.tasklist.userRole.UserRoleEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String login;
    private String name;
    private String lastName;
    private String password;
    private String mail;
    @ManyToMany
    @JoinTable(name="user_roles")
    private Set<UserRoleEntity> roles = new HashSet<>();
    @OneToMany(mappedBy = "userEntity")
    private List<TaskEntity> tasks = new ArrayList<>();

    public UserEntity(String login, String name, String lastName, String password, String mail) {
        this.login = login;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
        this.mail = mail;
    }


}
