package com.ewewerka.tasklist.user;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.register.RegisterForm;
import com.ewewerka.tasklist.userRole.Role;
import com.ewewerka.tasklist.userRole.UserRoleEntity;
import com.ewewerka.tasklist.userRole.UserRoleRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository roleRepository;
    private final UserRepository userRepository;

    public UserServiceImpl(PasswordEncoder passwordEncoder, UserRoleRepository roleRepository, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void createUser(RegisterForm registerForm) {
        String encodedPassword = passwordEncoder.encode(registerForm.getPassword());

        UserEntity userEntity = new UserEntity(registerForm.getLogin(),
                registerForm.getName(),
                registerForm.getLastName(),
                encodedPassword,
                registerForm.getMail());

        if (userEntity.getLogin().equalsIgnoreCase("admin@admin.pl")) {
            UserRoleEntity admin = roleRepository.findUserRoleEntityByRole(Role.ROLE_ADMIN.getName());
            userEntity.getRoles().add(admin);
        } else {
            UserRoleEntity user = roleRepository.findUserRoleEntityByRole(Role.ROLE_USER.getName());
            userEntity.getRoles().add(user);
        }

        userRepository.save(userEntity);

    }

    @Override
    public List<UserDto> registeredUsers() {
        return userRepository.findAll()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public boolean userExistByLogin(String login) {
        return userRepository.existsByLogin(login);
    }

    @Override
    public UserDto findUserByLogin(String login) throws NoUserException {
        return userRepository.getByLogin(login)
                .map(Mapper::map)
                .orElseThrow(() -> new NoUserException("No user found"));
    }

    @Override
    public int countAllUsers() {
        return userRepository.findAll().size();
    }
}
