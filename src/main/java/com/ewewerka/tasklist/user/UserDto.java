package com.ewewerka.tasklist.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserDto {

    private String login;
    private String name;
    private String lastName;
    private String password;
    private String mail;

}
