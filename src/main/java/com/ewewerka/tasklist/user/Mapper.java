package com.ewewerka.tasklist.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Mapper {

    public static UserDto map(UserEntity userEntity){
        return new UserDto(userEntity.getLogin(),
                userEntity.getName(),
                userEntity.getLastName(),
                userEntity.getPassword(),
                userEntity.getMail());
    }
}
