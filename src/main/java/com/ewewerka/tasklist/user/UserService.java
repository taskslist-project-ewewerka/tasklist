package com.ewewerka.tasklist.user;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.register.RegisterForm;

import java.util.List;

public interface UserService {
    void createUser(RegisterForm registerForm);

    List<UserDto> registeredUsers();

    boolean userExistByLogin(String login);

    UserDto findUserByLogin(String login) throws NoUserException;

    int countAllUsers();
}
