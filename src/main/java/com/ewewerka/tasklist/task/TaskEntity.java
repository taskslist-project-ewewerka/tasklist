package com.ewewerka.tasklist.task;

import com.ewewerka.tasklist.user.UserEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@Setter
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private Priority priority;
    @Enumerated(EnumType.STRING)
    private Status status = Status.OPEN;
    @ManyToOne
    @JoinColumn
    private UserEntity userEntity;


    public TaskEntity(String title, String description, Priority priority) {
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}
