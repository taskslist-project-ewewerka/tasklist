package com.ewewerka.tasklist.task;

import com.ewewerka.tasklist.exceptions.NoUserException;

import java.util.List;

public interface TaskService {
    List<TaskDto> getTasks();
    List<TaskDto> getTaskByUser() throws NoUserException;
    void addTask(TaskDto form) throws NoUserException;
    void removeTask(long  id);
    TaskDto getById(long id);
    TaskDto changeTask(TaskDto taskDto);
    TaskDto closeTask(long id);
}
