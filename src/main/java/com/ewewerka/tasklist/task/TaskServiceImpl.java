package com.ewewerka.tasklist.task;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.user.UserEntity;
import com.ewewerka.tasklist.user.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public TaskServiceImpl(TaskRepository taskRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<TaskDto> getTasks() {
       return taskRepository.findAll().stream().map(Mapper::map).collect(Collectors.toList());
    }

    @Override
    public List<TaskDto> getTaskByUser() throws NoUserException {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        UserEntity byUserLogin = userRepository.getByLogin(login)
                .orElseThrow(() -> new NoUserException("Task is not assigned to user"));
        return taskRepository.findAllByUserEntityId(byUserLogin.getId()).stream().map(Mapper::map).collect(Collectors.toList());
    }

    @Override
    public void addTask(TaskDto form) throws NoUserException {
        TaskEntity taskEntity = new TaskEntity(
                form.getTitle(),
                form.getDescription(),
                form.getPriority()
        );

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        UserEntity byUserLogin = userRepository.getByLogin(login)
                .orElseThrow(() -> new NoUserException("Task is not assigned to user"));
        taskEntity.setUserEntity(byUserLogin);
        taskRepository.save(taskEntity);
    }

    @Override
    public void removeTask(long id) {
        taskRepository.deleteById(id);
    }

    @Override
    public TaskDto getById(long id) {
        return taskRepository.findById(id).stream().map(Mapper::map).findFirst().get();//collect(Collectors.toList()).get(0);
    }

    @Override
    public TaskDto changeTask(TaskDto taskDto) {
        Optional<TaskEntity> task = taskRepository.findById(taskDto.getId());
        if(task.isPresent()){
            task.get().setTitle(taskDto.getTitle());
            task.get().setDescription(taskDto.getDescription());
            task.get().setPriority(taskDto.getPriority());
            TaskEntity updatedTask = taskRepository.save(task.get());
            return Mapper.map(updatedTask);
        }
        return null;
    }

    @Override
    public TaskDto closeTask(long id) {
       Optional<TaskEntity> task = taskRepository.findById(id);
        if(task.isPresent()){
            task.get().setStatus(Status.CLOSED);
            TaskEntity updatedTask = taskRepository.save(task.get());
            return Mapper.map(updatedTask);
        }
        return null;
    }

}
