package com.ewewerka.tasklist.task;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
@ToString
public class TaskDto {
    private Long id;
    private String title;
    private String description;
    private Priority priority;
    private Status status;

    public static List<TaskDto> firstOpenLastClosed(List<TaskDto> taskDtos) {
        List<TaskDto> list = new ArrayList<>();
        
        list.addAll(taskDtos.stream().filter(taskDto -> taskDto.getStatus().ordinal()==0).collect(Collectors.toList()));
        list.addAll(taskDtos.stream().filter(taskDto -> taskDto.getStatus().ordinal()==1).collect(Collectors.toList()));
        return list;
    }
}
