package com.ewewerka.tasklist.task;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<TaskEntity,Long> {
        List<TaskEntity> findAll();
        List<TaskEntity> findAllByUserEntityId(long id);
        void deleteById(long id);

}
