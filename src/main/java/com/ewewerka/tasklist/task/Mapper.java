package com.ewewerka.tasklist.task;

public final class Mapper {
    private Mapper() {

    }

    public static TaskDto map(TaskEntity taskEntity) {
        return new TaskDto(taskEntity.getId(),taskEntity.getTitle(),taskEntity.getDescription(),taskEntity.getPriority(),taskEntity.getStatus());
    }
}
