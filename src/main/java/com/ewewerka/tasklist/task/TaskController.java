package com.ewewerka.tasklist.task;

import com.ewewerka.tasklist.exceptions.NoUserException;
import com.ewewerka.tasklist.user.UserService;
import org.dom4j.rule.Mode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.net.ContentHandler;
import java.util.Arrays;
import java.util.List;

import static com.ewewerka.tasklist.task.TaskDto.firstOpenLastClosed;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;
    private final UserService userService;

    public TaskController(TaskService taskService, UserService userService) {
        this.taskService = taskService;
        this.userService = userService;
    }

    @RequestMapping
    ModelAndView getUsersTasks() throws NoUserException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        String name = userService.findUserByLogin(login).getName();
        ModelAndView mnv  = new ModelAndView("tasks");
        mnv.addObject("task", firstOpenLastClosed(taskService.getTaskByUser()));
        mnv.addObject("userName", name);
        return mnv;
    }

    @RequestMapping("/addtask")
    public ModelAndView getAddTaskPage() throws NoUserException {
        ModelAndView mnv = new ModelAndView("addtask");
        mnv.addObject("taskForm", new TaskDto());
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        mnv.addObject("userName", getUserName());
        List<Priority> priorities = Arrays.asList(Priority.LOW,Priority.MEDIUM,Priority.HIGH,Priority.DEADLY);
        mnv.addObject("priorities",priorities);
        return mnv;
    }

    @PostMapping("/addtask")
    String newTask(@ModelAttribute(name = "taskForm") TaskDto taskDto) throws NoUserException {
        taskService.addTask(taskDto);
        return "redirect:/tasks";
    }

    @RequestMapping("/{idTask}/remove")
    String removeTask(@PathVariable long idTask) {
        taskService.removeTask(idTask);
        return "redirect:/tasks";
    }

    @RequestMapping("/{idTask}/change")
    ModelAndView getChangeTaskPage(@PathVariable long idTask) throws NoUserException {
        TaskDto  task = taskService.getById(idTask);
        ModelAndView mnv = new ModelAndView("change");
        mnv.addObject("taskChangeForm", task);
        mnv.addObject("userName", getUserName());
        List<Priority> priorities = Arrays.asList(Priority.LOW,Priority.MEDIUM,Priority.HIGH,Priority.DEADLY);
        mnv.addObject("priorities",priorities);
        return mnv;
    }

    @PostMapping("/{idTask}/change")
    String changeTask(@ModelAttribute(name = "taskChangeForm") TaskDto taskChangeForm, @PathVariable long idTask) {
        taskService.changeTask(taskChangeForm);
        return "redirect:/tasks";
    }

    @RequestMapping("/{idTask}/close")
    String closeTask(@PathVariable long idTask){
        taskService.closeTask(idTask);
        return "redirect:/tasks";
    }

    private String getUserName() throws NoUserException {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return userService.findUserByLogin(login).getName();
    }
}
