package com.ewewerka.tasklist.config;

import com.ewewerka.tasklist.userRole.Role;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;
    private final PasswordEncoder passwordEncoder;

    public WebSecurityConfig(DataSource dataSource, PasswordEncoder passwordEncoder) {
        this.dataSource = dataSource;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //TODO:make authorisation (any request should be last?)
        http.authorizeRequests()
                .antMatchers("/index").permitAll()
                .antMatchers("/h2/**").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/tasks/**").permitAll()
                .antMatchers("/admin/**").hasAnyAuthority(Role.ROLE_ADMIN.getName())
                .and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                .loginPage("/login") //przekierowanie na kontroler
                .defaultSuccessUrl("/main")
                .failureUrl("/login?error=true")
                .permitAll();


        //http.authorizeRequests().anyRequest().permitAll();
    }

    //metoda do autentykacji
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication().usersByUsernameQuery("SELECT u.login, u.password,1 FROM user_entity u WHERE u.login=?")
                .authoritiesByUsernameQuery("SELECT u.login, role.role, 1 "
                        + "FROM user_entity u "
                        + "INNER JOIN user_roles ur ON ur.user_entity_id = u.id "
                        + "INNER JOIN user_role_entity role ON role.id = ur.roles_id "
                        + "WHERE u.login=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }


}
