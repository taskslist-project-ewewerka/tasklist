package com.ewewerka.tasklist.mock;

import com.ewewerka.tasklist.task.Priority;
import com.ewewerka.tasklist.task.TaskEntity;
import com.ewewerka.tasklist.task.TaskRepository;
import com.ewewerka.tasklist.user.UserEntity;
import com.ewewerka.tasklist.user.UserRepository;
import com.ewewerka.tasklist.userRole.Role;
import com.ewewerka.tasklist.userRole.UserRoleEntity;
import com.ewewerka.tasklist.userRole.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class MockData {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostConstruct
    public void mockData(){
        if(!userRepository.existsByLogin("user@user.pl")) {
            UserRoleEntity role = userRoleRepository.findUserRoleEntityByRole(Role.ROLE_USER.getName());

            if(role == null) {
                role = userRoleRepository.save(new UserRoleEntity(Role.ROLE_USER.getName()));
            }

            UserEntity user = new UserEntity("user@user.pl","Adam","Pieniawski",passwordEncoder.encode("user"),"user@user.pl");
            user.getRoles().add(role);

            userRepository.save(user);

            TaskEntity task = new TaskEntity("Zrobić tasklist","Zrób piękną tasklistę", Priority.LOW);
            task.setUserEntity(user);

            taskRepository.save(task);
        }

        if(!userRepository.existsByLogin("admin")) {
            UserRoleEntity roleAdmin = userRoleRepository.findUserRoleEntityByRole(Role.ROLE_ADMIN.getName());

            if(roleAdmin == null) {
                roleAdmin = userRoleRepository.save(new UserRoleEntity(Role.ROLE_ADMIN.getName()));
            }

            UserEntity userAdmin = new UserEntity("admin",
                    "Adam","Pieniawski",
                    passwordEncoder.encode("admin"),
                    "admin@admin.pl");
            userAdmin.getRoles().add(roleAdmin);

            userRepository.save(userAdmin);

            TaskEntity taskAdmin= new TaskEntity("Zarządzać tasklistą","Zarządzić piękną tasklistą", Priority.DEADLY);
            taskAdmin.setUserEntity(userAdmin);

            taskRepository.save(taskAdmin);
        }


    }
}
