package com.ewewerka.tasklist.userRole;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRoleRepository extends CrudRepository<UserRoleEntity, Long> {
    boolean existsByRole (String role);

    UserRoleEntity findUserRoleEntityByRole (String role);

}
