package com.ewewerka.tasklist.userRole;

import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@NoArgsConstructor
public class UserRoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String role;

    public UserRoleEntity(String role) {
        this.role = role;
    }
}
