package com.ewewerka.tasklist.exceptions;

public class NoUserException extends WebAppException{
    public NoUserException(String message) {
        super(message);
    }
}
