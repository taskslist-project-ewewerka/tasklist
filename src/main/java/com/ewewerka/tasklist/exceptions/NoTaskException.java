package com.ewewerka.tasklist.exceptions;

public class NoTaskException extends WebAppException{

    public NoTaskException(String message) {
        super(message);
    }
}
